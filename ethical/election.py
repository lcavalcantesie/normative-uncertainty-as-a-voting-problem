import numpy as np
import pandas as pd
from .classifier import *
from .principles import *
from tqdm import tqdm, tqdm_notebook

#welcome to the Imaginary Parliament of Ethics



        ###############
        ## CREDENCES ##
        ###############


def credency(x, converted=False):
    if converted:
        #p,a = moral_profile(x, converted=True)
        p,a = moral_profile(x, converted=True)
        y = moral_profile2(p,a)
    else:
        #p,a = moral_Profile(x)
        p,a = moral_profile(x)
        y = moral_profile2(p,a)
    #new dataframe for vote
    y = y[['HowMuch_SentientUtilitarianism','Ambiguity_SentientUtilitarianism','HowMuch_ClassicalUtilitarianism','Ambiguity_ClassicalUtilitarianism','HowMuch_HedonisticUtilitarianism','Ambiguity_HedonisticUtilitarianism','HowMuch_AgentCenteredDeontology','Ambiguity_AgentCenteredDeontology','HowMuch_PatientCenteredDeontology','Ambiguity_PatientCenteredDeontology','HowMuch_ContractarianDeontology','Ambiguity_ContractarianDeontology']]
    #let's invert all ambiguity in order to directly multiply
    y['Ambiguity_SentientUtilitarianism'] = y['Ambiguity_SentientUtilitarianism'].apply(lambda x: 1-x)
    y['Ambiguity_ClassicalUtilitarianism'] = y['Ambiguity_ClassicalUtilitarianism'].apply(lambda x: 1-x)
    y['Ambiguity_HedonisticUtilitarianism'] = y['Ambiguity_HedonisticUtilitarianism'].apply(lambda x: 1-x)
    y['Ambiguity_AgentCenteredDeontology'] = y['Ambiguity_AgentCenteredDeontology'].apply(lambda x: 1-x)
    y['Ambiguity_PatientCenteredDeontology'] = y['Ambiguity_PatientCenteredDeontology'].apply(lambda x: 1-x)
    y['Ambiguity_ContractarianDeontology'] = y['Ambiguity_ContractarianDeontology'].apply(lambda x: 1-x)
    #let's multiply
    y['Sentient_U_credence'] = y['Ambiguity_SentientUtilitarianism'] * y['HowMuch_SentientUtilitarianism']
    y['Classical_U_credence'] = y['Ambiguity_ClassicalUtilitarianism'] * y['HowMuch_ClassicalUtilitarianism']
    y['Hedonistic_U_credence'] = y['Ambiguity_HedonisticUtilitarianism'] * y['HowMuch_HedonisticUtilitarianism']
    y['AgentCentered_D_credence'] = y['Ambiguity_AgentCenteredDeontology'] * y['HowMuch_AgentCenteredDeontology']
    y['PatientCentered_D_credence'] = y['Ambiguity_PatientCenteredDeontology'] * y['HowMuch_PatientCenteredDeontology']
    y['Contractarian_D_credence'] = y['Ambiguity_ContractarianDeontology'] * y['HowMuch_ContractarianDeontology']
    #then keeping the relevent criteria
    #y = y.reset_index()
    result = y[['Sentient_U_credence','Classical_U_credence','Hedonistic_U_credence','AgentCentered_D_credence','PatientCentered_D_credence','Contractarian_D_credence']]
    return result


        ###############
        ##    MFT    ##
        ###############
        #that is, the principle with the highest credency gets to decide

def my_favourite_theory_utility(x):
    y = x
    if y.Favourite_theory == 'Sentient_U_credence':
        return SentientUtilitarianism_utility(y)
    if y.Favourite_theory == 'Classical_U_credence':
        return ClassicalUtilitarianism_utility(y)
    if y.Favourite_theory == 'Hedonistic_U_credence':
        return HedonisticUtilitarianism_utility(y)
    if y.Favourite_theory == 'AgentCentered_D_credence':
        return AgentCenteredDeontology_utility(y)
    if y.Favourite_theory == 'PatientCentered_D_credence':
        return PatientCenteredDeontology_utility(y)
    if y.Favourite_theory == 'Contractarian_D_credence':
        return ContractarianDeontology_utility(y)

def my_favourite_theory_count(x, converted=False, evaluation=False):

    if converted:
        y = x
    else:
        y = conversion(x)

    credences = credency(y, converted=True)
    credences['Favourite_theory'] = credences.idxmax(axis=1)
    credences['Credency'] = credences.max(axis=1)
    result = y.set_index('UserID', drop=True).merge(credences, left_index=True, right_index=True)

    #this just enables a progress bar to appear while processing, not necessary but useful
    tqdm_notebook().pandas()
    result['MFT_score'] = result.progress_apply(my_favourite_theory_utility, axis=1)
    if evaluation:
        return result
    else:
        #replace -1 by 0
        result['MFT_score'] = result['MFT_score'].progress_apply(lambda x: 0 if x == -1 else x)
        return result


          ###############
    ##  INTERMEDIATE FUCTIONS ##
          ###############


def vote_Sentient_U(x, converted=False, vote='majority'):
    #Utilitarian principle 1 - saves the most lives, whether humans or pets
    #
    ##the 'converted' argument indicates if the conversion function had previously been applied
    ##this is useful when I want to deal with one specific moral principle without having to deal with useless column
    ##that means that by default, it converts the dataframe and outputs columns that are relevant for this specific moral principle only
    ##the 'converted' argument is only meant to be true when used in the moral_profile function
    #
    if converted:
        y = x
    else:
        y = conversion(x, 'SentientUtilitarianism')
    #this just enables a progress bar to appear while processing, not necessary but useful
    tqdm_notebook().pandas()
    y['Sentient_U_chosen'] = y.progress_apply(SentientUtilitarianism_utility, axis=1)
    if vote=='borda':
        ##invert -1 and 0 meanings
        print('Returning borda count')
        y['Sentient_U_chosen'] = y['Sentient_U_chosen'].progress_apply(lambda x: 0 if x == -1 else (-1 if x == 0 else x))
        y['Sentient_U_unchosen'] = y['Sentient_U_chosen'].progress_apply(lambda x: 1 if x == -1 else (-1 if x == 1 else x))
    else:
        print('Returning default choice - simple majority vote')
        y['Sentient_U_chosen'] = y['Sentient_U_chosen'].progress_apply(lambda x: 0 if x == -1 else x)
        y['Sentient_U_unchosen'] = y['Sentient_U_chosen'].progress_apply(lambda x: 1 if x == 0 else (0 if x == 1 else x))
    return y


def vote_Classical_U(x, converted=False, vote='majority'):
    #Utilitarian principle 2 - saves the most human lives
    #
    ##the 'converted' argument indicates if the conversion function had previously been applied
    ##this is useful when I want to deal with one specific moral principle without having to deal with useless column
    ##that means that by default, it converts the dataframe and outputs columns that are relevant for this specific moral principle only
    ##the 'converted' argument is only meant to be true when used in the moral_profile function
    #
    if converted:
        y = x
    else:
        y = conversion(x, 'ClassicalUtilitarianism')
    #this just enables a progress bar to appear while processing, not necessary but useful
    tqdm_notebook().pandas()
    y['Classical_U_chosen'] = y.progress_apply(ClassicalUtilitarianism_utility, axis=1)
    if vote=='borda':
        ##invert -1 and 0 meanings
        print('Returning borda count')
        y['Classical_U_chosen'] = y['Classical_U_chosen'].progress_apply(lambda x: 0 if x == -1 else (-1 if x == 0 else x))
        y['Classical_U_unchosen'] = y['Classical_U_chosen'].progress_apply(lambda x: 1 if x == -1 else (-1 if x == 1 else x))
    else:
        print('Returning default choice - simple majority vote')
        y['Classical_U_chosen'] = y['Classical_U_chosen'].progress_apply(lambda x: 0 if x == -1 else x)
        y['Classical_U_unchosen'] = y['Classical_U_chosen'].progress_apply(lambda x: 1 if x == 0 else (0 if x == 1 else x))
    return y


def vote_Hedonistic_U(x, converted=False, vote='majority'):
    #Utilitarian principle 3 - saves the most lives in terms of life expectancy
    #
    ##the 'converted' argument indicates if the conversion function had previously been applied
    ##this is useful when I want to deal with one specific moral principle without having to deal with useless column
    ##that means that by default, it converts the dataframe and outputs columns that are relevant for this specific moral principle only
    ##the 'converted' argument is only meant to be true when used in the moral_profile function
    #
    if converted:
        y = x
    else:
        y = conversion(x, 'HedonisticUtilitarianism')
    #this just enables a progress bar to appear while processing, not necessary but useful
    tqdm_notebook().pandas()
    y['Hedonistic_U_chosen'] = y.progress_apply(HedonisticUtilitarianism_utility, axis=1)
    if vote=='borda':
        ##invert -1 and 0 meanings
        print('Returning borda count')
        ##'Ambiguity_HedonisticUtilitarianism', which differentiate the situation with ties -- =0 if tie, else =1
        y['Hedonistic_U_chosen'] = y['Hedonistic_U_chosen'].progress_apply(lambda x: 0 if x == -1 else (-1 if x == 0 else x))
        y['Hedonistic_U_unchosen'] = y['Hedonistic_U_chosen'].progress_apply(lambda x: 1 if x == -1 else (-1 if x == 1 else x))
    else:
        print('Returning default choice - simple majority vote')
        y['Hedonistic_U_chosen'] = y['Hedonistic_U_chosen'].progress_apply(lambda x: 0 if x == -1 else x)
        y['Hedonistic_U_unchosen'] = y['Hedonistic_U_chosen'].progress_apply(lambda x: 1 if x == 0 else (0 if x == 1 else x))
    return y


def vote_AgentCentered_D(x, converted=False, vote='majority'):
    #AgentCenteredDeontology principle - duty ethics, chooses self-sacrifice over involving pedestrians
    #
    ##the 'converted' argument indicates if the conversion function had previously been applied
    ##this is useful when I want to deal with one specific moral principle without having to deal with useless column
    ##that means that by default, it converts the dataframe and outputs columns that are relevant for this specific moral principle only
    ##the 'converted' argument is only meant to be true when used in the moral_profile function
    #
    if converted:
        y = x
    else:
        y = conversion(x, 'AgentCenteredDeontology')
    #this just enables a progress bar to appear while processing, not necessary but useful
    tqdm_notebook().pandas()
    y['AgentCentered_D_chosen'] = y.progress_apply(AgentCenteredDeontology_utility, axis=1)
    if vote=='borda':
        ##invert -1 and 0 meanings
        print('Returning borda count')
        ##'Ambiguity_AgentCenteredDeontology', which differentiate the situation with ties -- =0 if tie, else =1
        y['AgentCentered_D_chosen'] = y['AgentCentered_D_chosen'].progress_apply(lambda x: 0 if x == -1 else (-1 if x == 0 else x))
        y['AgentCentered_D_unchosen'] = y['AgentCentered_D_chosen'].progress_apply(lambda x: 1 if x == -1 else (-1 if x == 1 else x))
    else:
        print('Returning default choice - simple majority vote')
        y['AgentCentered_D_chosen'] = y['AgentCentered_D_chosen'].progress_apply(lambda x: 0 if x == -1 else x)
        y['AgentCentered_D_unchosen'] = y['AgentCentered_D_chosen'].progress_apply(lambda x: 1 if x == 0 else (0 if x == 1 else x))
    return y


def vote_PatientCentered_D(x, converted=False, vote='majority'):
    #PatientCenteredDeontology principle - rights-based ethics, chooses self-sacrifice over involving pedestrians, when not possible avoid swerving the car
    #
    ##the 'converted' argument indicates if the conversion function had previously been applied
    ##this is useful when I want to deal with one specific moral principle without having to deal with useless column
    ##that means that by default, it converts the dataframe and outputs columns that are relevant for this specific moral principle only
    ##the 'converted' argument is only meant to be true when used in the moral_profile function
    #
    if converted:
        y = x
    else:
        y = conversion(x, 'PatientCenteredDeontology')
    #this just enables a progress bar to appear while processing, not necessary but useful
    tqdm_notebook().pandas()
    y['PatientCentered_D_chosen'] = y.progress_apply(PatientCenteredDeontology_utility, axis=1)
    if vote=='borda':
        ##invert -1 and 0 meanings
        print('Returning borda count')
        y['PatientCentered_D_chosen'] = y['PatientCentered_D_chosen'].progress_apply(lambda x: 0 if x == -1 else (-1 if x == 0 else x))
        y['PatientCentered_D_unchosen'] = y['PatientCentered_D_chosen'].progress_apply(lambda x: 1 if x == -1 else (-1 if x == 1 else x))
    else:
        print('Returning default choice - simple majority vote')
        y['PatientCentered_D_chosen'] = y['PatientCentered_D_chosen'].progress_apply(lambda x: 0 if x == -1 else x)
        y['PatientCentered_D_unchosen'] = y['PatientCentered_D_chosen'].progress_apply(lambda x: 1 if x == 0 else (0 if x == 1 else x))
    return y


def vote_Contractarian_D(x, converted=False, vote='majority'):
    #ContractarianDeontology principle - duty ethics, chooses to save pedestrians legally crossing the street whenever possible
    #
    ##the 'converted' argument indicates if the conversion function had previously been applied
    ##this is useful when I want to deal with one specific moral principle without having to deal with useless column
    ##that means that by default, it converts the dataframe and outputs columns that are relevant for this specific moral principle only
    ##the 'converted' argument is only meant to be true when used in the moral_profile function
    #
    if converted:
        y = x
    else:
        y = conversion(x, 'ContractarianDeontology')
    #this just enables a progress bar to appear while processing, not necessary but useful
    tqdm_notebook().pandas()
    y['Contractarian_D_chosen'] = y.progress_apply(ContractarianDeontology_utility, axis=1)
    if vote=='borda':
        ##invert -1 and 0 meanings
        print('Returning borda count')
        y['Contractarian_D_chosen'] = y['Contractarian_D_chosen'].progress_apply(lambda x: 0 if x == -1 else (-1 if x == 0 else x))
        y['Contractarian_D_unchosen'] = y['Contractarian_D_chosen'].progress_apply(lambda x: 1 if x == -1 else (-1 if x == 1 else x))
    else:
        print('Returning default choice - simple majority vote')
        y['Contractarian_D_chosen'] = y['Contractarian_D_chosen'].progress_apply(lambda x: 0 if x == -1 else x)
        y['Contractarian_D_unchosen'] = y['Contractarian_D_chosen'].progress_apply(lambda x: 1 if x == 0 else (0 if x == 1 else x))
    return y


        ###############
        ##   BORDA   ##
        ###############


def borda_evaluation(x):
    y = x
    if y.BordaScore_chosen > y.BordaScore_unchosen:
        return 1
    else:
        return 0

def borda_count(x, converted=False, without_credences=False):
    #outputs a dataframe with a column for each moral principle

    ##first converts the dataframe to have the maximum info possible - if not already converted
    if converted:
        y = x
    else:
        y = conversion(x)

    ##then applies the moral principles
    u_Sentient = vote_Sentient_U(y, converted = True, vote='borda')
    u_Classical = vote_Classical_U(y, converted = True, vote='borda')
    u_Hedonistic = vote_Hedonistic_U(y, converted = True, vote='borda')
    d_AgentCentered = vote_AgentCentered_D(y, converted = True, vote='borda')
    d_PatientCentered = vote_PatientCentered_D(y, converted = True, vote='borda')
    d_Contractarian = vote_Contractarian_D(y, converted = True, vote='borda')
    #join all of them into one magical dataframe
    globalprinciples = [u_Sentient, u_Classical, u_Hedonistic, d_AgentCentered, d_PatientCentered, d_Contractarian]
    result_principles = pd.concat(globalprinciples, axis=1, join='inner')
    #drop doublons
    result_principles = result_principles.loc[:,~result_principles.columns.duplicated()]
    #set UserID as index
    result_principles = result_principles.set_index('UserID', drop=True)

    if without_credences:
        result = result_principles
        result['Sentient_U_credence'] = 1
        result['Classical_U_credence'] = 1
        result['Hedonistic_U_credence'] = 1
        result['PatientCentered_D_credence'] = 1
        result['AgentCentered_D_credence'] = 1
        result['Contractarian_D_credence'] = 1
    else:
        credences = credency(y, converted=True)
        result = result_principles.merge(credences, left_index=True, right_index=True)

    ##outputs borda scores for both chosen and unchosen answers
    result['BordaScore_chosen'] = result['Sentient_U_credence'] * result['Sentient_U_chosen'] + result['Classical_U_credence'] * result['Classical_U_chosen'] + result['Hedonistic_U_credence'] * result['Hedonistic_U_chosen'] + result['PatientCentered_D_credence'] * result['PatientCentered_D_chosen'] + result['AgentCentered_D_credence'] * result['AgentCentered_D_chosen'] + result['Contractarian_D_credence'] * result['Contractarian_D_chosen']
    result['BordaScore_unchosen'] = result['Sentient_U_credence'] * result['Sentient_U_unchosen'] + result['Classical_U_credence'] * result['Classical_U_unchosen'] + result['Hedonistic_U_credence'] * result['Hedonistic_U_unchosen'] + result['PatientCentered_D_credence'] * result['PatientCentered_D_unchosen'] + result['AgentCentered_D_credence'] * result['AgentCentered_D_unchosen'] + result['Contractarian_D_credence'] * result['Contractarian_D_unchosen']
    tqdm_notebook().pandas()
    result['Borda_wins'] = result.progress_apply(borda_evaluation, axis=1)
    return result


        ################
        ##  MAJORITY  ##
        ################


def majority_evaluation(x):
    y = x
    if y.MajorityScore_chosen > y.MajorityScore_unchosen:
        return 1
    else:
        return 0


def majority_count(x, converted=False, without_credences=False):

    if converted:
        y = x
    else:
        y = conversion(x)

    ##then applies the moral principles
    u_Sentient = vote_Sentient_U(y, converted = True)
    u_Classical = vote_Classical_U(y, converted = True)
    u_Hedonistic = vote_Hedonistic_U(y, converted = True)
    d_AgentCentered = vote_AgentCentered_D(y, converted = True)
    d_PatientCentered = vote_PatientCentered_D(y, converted = True)
    d_Contractarian = vote_Contractarian_D(y, converted = True)
    #join all of them into one magical dataframe
    globalprinciples = [u_Sentient, u_Classical, u_Hedonistic, d_AgentCentered, d_PatientCentered, d_Contractarian]
    result_principles = pd.concat(globalprinciples, axis=1, join='inner')
    #drop doublons
    result_principles = result_principles.loc[:,~result_principles.columns.duplicated()]
    #set UserID as index
    result_principles = result_principles.set_index('UserID', drop=True)

    if without_credences:
        result = result_principles
        result['Sentient_U_credence'] = 1
        result['Classical_U_credence'] = 1
        result['Hedonistic_U_credence'] = 1
        result['PatientCentered_D_credence'] = 1
        result['AgentCentered_D_credence'] = 1
        result['Contractarian_D_credence'] = 1
    else:
        credences = credency(y, converted=True)
        result = result_principles.merge(credences, left_index=True, right_index=True)

    ##outputs borda scores for both chosen and unchosen answers
    result['MajorityScore_chosen'] = result['Sentient_U_credence'] * result['Sentient_U_chosen'] + result['Classical_U_credence'] * result['Classical_U_chosen'] + result['Hedonistic_U_credence'] * result['Hedonistic_U_chosen'] + result['PatientCentered_D_credence'] * result['PatientCentered_D_chosen'] + result['AgentCentered_D_credence'] * result['AgentCentered_D_chosen'] + result['Contractarian_D_credence'] * result['Contractarian_D_chosen']
    result['MajorityScore_unchosen'] = result['Sentient_U_credence'] * result['Sentient_U_unchosen'] + result['Classical_U_credence'] * result['Classical_U_unchosen'] + result['Hedonistic_U_credence'] * result['Hedonistic_U_unchosen'] + result['PatientCentered_D_credence'] * result['PatientCentered_D_unchosen'] + result['AgentCentered_D_credence'] * result['AgentCentered_D_unchosen'] + result['Contractarian_D_credence'] * result['Contractarian_D_unchosen']
    tqdm_notebook().pandas()
    result['Majority_wins'] = result.progress_apply(majority_evaluation, axis=1)
    return result


          #################
        ##  SOCIETY LEVEL  ##
          #################
        # that is, average credences applied at individual level

## borda

def society_borda_count(x, converted=False):
    #outputs a dataframe with a column for each moral principle

    ##first converts the dataframe to have the maximum info possible - if not already converted
    if converted:
        y = x
    else:
        y = conversion(x)

    ##then applies the moral principles
    u_Sentient = vote_Sentient_U(y, converted = True, vote='borda')
    u_Classical = vote_Classical_U(y, converted = True, vote='borda')
    u_Hedonistic = vote_Hedonistic_U(y, converted = True, vote='borda')
    d_AgentCentered = vote_AgentCentered_D(y, converted = True, vote='borda')
    d_PatientCentered = vote_PatientCentered_D(y, converted = True, vote='borda')
    d_Contractarian = vote_Contractarian_D(y, converted = True, vote='borda')
    #join all of them into one magical dataframe
    globalprinciples = [u_Sentient, u_Classical, u_Hedonistic, d_AgentCentered, d_PatientCentered, d_Contractarian]
    result_principles = pd.concat(globalprinciples, axis=1, join='inner')
    #drop doublons
    result_principles = result_principles.loc[:,~result_principles.columns.duplicated()]
    #set UserID as index
    result_principles = result_principles.set_index('UserID', drop=True)
    #do the same with credences
    credences = credency(y, converted=True)
    result = result_principles.merge(credences, left_index=True, right_index=True)
    ##outputs borda scores for both chosen and unchosen answers
    result['BordaScore_chosen'] = result['Sentient_U_credence'].mean() * result['Sentient_U_chosen'] + result['Classical_U_credence'].mean() * result['Classical_U_chosen'] + result['Hedonistic_U_credence'].mean() * result['Hedonistic_U_chosen'].mean() + result['PatientCentered_D_credence'].mean() * result['PatientCentered_D_chosen'] + result['AgentCentered_D_credence'].mean() * result['AgentCentered_D_chosen'] + result['Contractarian_D_credence'] * result['Contractarian_D_chosen']
    result['BordaScore_unchosen'] = result['Sentient_U_credence'].mean() * result['Sentient_U_unchosen'] + result['Classical_U_credence'].mean() * result['Classical_U_unchosen'] + result['Hedonistic_U_credence'].mean() * result['Hedonistic_U_unchosen'] + result['PatientCentered_D_credence'].mean() * result['PatientCentered_D_unchosen'] + result['AgentCentered_D_credence'].mean() * result['AgentCentered_D_unchosen'] + result['Contractarian_D_credence'].mean() * result['Contractarian_D_unchosen']
    tqdm_notebook().pandas()
    result['Borda_wins'] = result.progress_apply(borda_evaluation, axis=1)
    return result


## majority

def society_majority_count(x, converted=False):

    if converted:
        y = x
    else:
        y = conversion(x)

    ##then applies the moral principles
    u_Sentient = vote_Sentient_U(y, converted = True)
    u_Classical = vote_Classical_U(y, converted = True)
    u_Hedonistic = vote_Hedonistic_U(y, converted = True)
    d_AgentCentered = vote_AgentCentered_D(y, converted = True)
    d_PatientCentered = vote_PatientCentered_D(y, converted = True)
    d_Contractarian = vote_Contractarian_D(y, converted = True)
    #join all of them into one magical dataframe
    globalprinciples = [u_Sentient, u_Classical, u_Hedonistic, d_AgentCentered, d_PatientCentered, d_Contractarian]
    result_principles = pd.concat(globalprinciples, axis=1, join='inner')
    #drop doublons
    result_principles = result_principles.loc[:,~result_principles.columns.duplicated()]
    #set UserID as index
    result_principles = result_principles.set_index('UserID', drop=True)
    #do the same with credences
    credences = credency(y, converted=True)
    result = result_principles.merge(credences, left_index=True, right_index=True)
    ##outputs borda scores for both chosen and unchosen answers
    result['MajorityScore_chosen'] = result['Sentient_U_credence'].mean() * result['Sentient_U_chosen'] + result['Classical_U_credence'].mean() * result['Classical_U_chosen'] + result['Hedonistic_U_credence'].mean() * result['Hedonistic_U_chosen'] + result['PatientCentered_D_credence'].mean() * result['PatientCentered_D_chosen'] + result['AgentCentered_D_credence'].mean() * result['AgentCentered_D_chosen'] + result['Contractarian_D_credence'].mean() * result['Contractarian_D_chosen']
    result['MajorityScore_unchosen'] = result['Sentient_U_credence'].mean() * result['Sentient_U_unchosen'] + result['Classical_U_credence'].mean() * result['Classical_U_unchosen'] + result['Hedonistic_U_credence'].mean() * result['Hedonistic_U_unchosen'] + result['PatientCentered_D_credence'].mean() * result['PatientCentered_D_unchosen'] + result['AgentCentered_D_credence'].mean() * result['AgentCentered_D_unchosen'] + result['Contractarian_D_credence'].mean() * result['Contractarian_D_unchosen']
    tqdm_notebook().pandas()
    result['Majority_wins'] = result.progress_apply(majority_evaluation, axis=1)
    return result


## my favourite theory

def mft_society_utility(x):
    y = x
    if y.idxmax(axis=1) == 'Sentient_U_credence':
        return 'Sentient_U_credence'
    if y.idxmax(axis=1) == 'Classical_U_credence':
        return 'Classical_U_credence'
    if y.idxmax(axis=1) == 'Hedonistic_U_credence':
        return 'Hedonistic_U_credence'
    if y.idxmax(axis=1) == 'AgentCentered_D_credence':
        return 'AgentCentered_D_credence'
    if y.idxmax(axis=1) == 'PatientCentered_D_credence':
        return 'PatientCentered_D_credence'
    if y.idxmax(axis=1) == 'Contractarian_D_credence':
        return 'Contractarian_D_credence'

def society_my_favourite_theory_count(x, converted=False, evaluation=False):

    if converted:
        y = x
    else:
        y = conversion(x)

    credences = credency(y, converted=True)
    result = y.set_index('UserID', drop=True).merge(credences, left_index=True, right_index=True)
    result['Credency'] = max(result['Sentient_U_credence'].mean(), result['Classical_U_credence'].mean(), result['Hedonistic_U_credence'].mean(), result['AgentCentered_D_credence'].mean(), result['PatientCentered_D_credence'].mean(), result['Contractarian_D_credence'].mean())
    result['Favourite_theory'] = 'PatientCentered_D_credence'
    tqdm_notebook().pandas()
    result['MFT_score'] = result.progress_apply(my_favourite_theory_utility, axis=1)
    #this just enables a progress bar to appear while processing, not necessary but useful
    if evaluation:
        return result
    else:
        #replace -1 by 0
        result['MFT_score'] = result['MFT_score'].progress_apply(lambda x: 0 if x == -1 else x)
        return result
