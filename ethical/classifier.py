import numpy as np
import pandas as pd

#these functions are classfiers in order to regroup most relevant data as criterias for each moral principles

#here are the different types of lives in the original db:
#'Man', 'Woman', 'Pregnant', 'Stroller',
#'OldMan', 'OldWoman', 'Boy', 'Girl', 'Homeless', 'LargeWoman',
#'LargeMan', 'Criminal', 'MaleExecutive', 'FemaleExecutive',
#'FemaleAthlete', 'MaleAthlete', 'FemaleDoctor', 'MaleDoctor',
#'Dog','Cat'
#
#also, here are some useful information about the environment:
#'PedPed', 'Barrier', 'CrossingSignal'
#
#finally, there are the information concerning the user:
#'ResponseID', 'ExtendedSessionID', 'UserID', 'ScenarioOrder',
#'Intervention'

#each classifer function takes the original dataframe as input and outputs a new dataframe with relevant categories for each moral principle

def conversion(x, specific=''):
    #general conversion function
    #
    ##the 'specific' argument is useful when I want to deal with one specific moral principle without having to deal with useless column
    ##by default, all columns are kept
    #
    #y = x.copy(deep=True)
    y = x.copy(deep=True)
    #output new columns:
    ##'Human', which contains the number of Human lives
    y['Human'] = y['Man'].astype(float).astype(int) + y['Woman'].astype(int) + y['Pregnant'].astype(int) + y['Stroller'].astype(int) + y['OldMan'].astype(int) + y['OldWoman'].astype(int) + y['Boy'].astype(int) + y['Girl'].astype(int) + y['Homeless'].astype(int) + y['LargeWoman'].astype(int) + y['LargeMan'].astype(int) + y['Criminal'].astype(int) + y['MaleExecutive'].astype(int) + y['FemaleExecutive'].astype(int) + y['FemaleAthlete'].astype(int) + y['MaleAthlete'].astype(int) + y['FemaleDoctor'].astype(int) + y['MaleDoctor'].astype(int)
    ##'Pets', which contains the number of Pets lives
    y['Pets'] = y['Dog'].astype(int) + y['Cat'].astype(int)
    ##'Lives', which contains the number of lives
    y['Lives'] = y['Human'] + y['Pets']
    ##'Kid', which contains the number of kids
    y['Kid'] = y['Boy'].astype(float).astype(int) + y['Girl'].astype(float).astype(int)
    #'Adult', which contains the number of adults
    y['Adult'] = y['Man'].astype(float).astype(int) + y['Woman'].astype(float).astype(int) + y['Pregnant'].astype(float).astype(int) + y['Stroller'].astype(float).astype(int) + y['Homeless'].astype(float).astype(int) + y['LargeWoman'].astype(float).astype(int) + y['LargeMan'].astype(float).astype(int) + y['Criminal'].astype(float).astype(int) + y['MaleExecutive'].astype(float).astype(int) + y['FemaleExecutive'].astype(float).astype(int) + y['FemaleAthlete'].astype(float).astype(int) + y['MaleAthlete'].astype(float).astype(int) + y['FemaleDoctor'].astype(float).astype(int) + y['MaleDoctor'].astype(float).astype(int)
    #'Senior', which contains the number of seniors
    y['Senior'] = y['OldMan'].astype(float).astype(int) + y['OldWoman'].astype(float).astype(int)
    ##we assume life expectancy for each individuals
    LifeExpectancy = 85
    ##we assume kids are 10, adults are 35 and seniors are 60
    kids_age = 10
    adults_age = 35
    seniors_age = 60
    #'Years', the number of years left for each individual
    y['Years'] = (y['Kid'] * (LifeExpectancy - kids_age)) + (y['Adult'] * (LifeExpectancy - adults_age)) + (y['Senior'] * (LifeExpectancy - seniors_age))
    #okay so some of the columns are not needed, let's define the relevant ones only
    categories = ["ResponseID","UserID","UserCountry3","ScenarioOrder","Intervention","Barrier","CrossingSignal","Saved","Lives","Human","Years","Pets"]
    #we split the database in two
    ##1st half : the lives the respondent chose to save
    util1_saved = y[y.Saved =='1'][categories]
    util1_saved = util1_saved.sort_values(by=["UserID","ScenarioOrder"])
    ##2nd half : the lives the respondent chose not to save
    util1_notsaved = y[y.Saved =='0'][categories]
    util1_notsaved = util1_notsaved.sort_values(by=["UserID","ScenarioOrder"])
    #join the two databases, and specifying the ones he saved with the suffix '_saved' and the ones he did not with the suffix '_notsaved'
    result = util1_saved.join(util1_notsaved.set_index('ResponseID'), on='ResponseID', lsuffix='_saved', rsuffix='_notsaved', how='inner')
    #keeping the only interesting criterias:
    ##all of these condition statements is just for debugging, when I need to access a specific moral principle and I don't want to deal with all of the column
    ##by default, all columns are kept
    result = result.rename(columns={"UserID_saved": "UserID", "ScenarioOrder_saved": "ScenarioOrder", "UserCountry3_saved": "UserCountry"})
    result = result.drop(columns=['UserID_notsaved','ScenarioOrder_notsaved','UserCountry3_notsaved','Saved_saved','Saved_notsaved'])
    if specific == 'SentientUtilitarianism':
        result = result[['UserID','UserCountry','ScenarioOrder','Lives_saved','Lives_notsaved']]
    elif specific == 'ClassicalUtilitarianism':
        result = result[['UserID','UserCountry','ScenarioOrder','Human_saved','Human_notsaved','Pets_saved','Pets_notsaved']]
    elif specific == 'HedonisticUtilitarianism':
        result = result[['UserID','UserCountry','ScenarioOrder','Years_saved','Years_notsaved']]
    elif specific == 'AgentCenteredDeontology':
        result = result[['UserID','UserCountry','ScenarioOrder','Barrier_saved','Barrier_notsaved']]
    elif specific == 'PatientCenteredDeontology':
        result = result[['UserID','UserCountry','ScenarioOrder','Barrier_saved','Barrier_notsaved','Intervention_saved','Intervention_notsaved']]
    elif specific == 'ContractarianDeontology':
        result = result[['UserID','UserCountry','ScenarioOrder','Barrier_saved','Barrier_notsaved','CrossingSignal_saved','CrossingSignal_notsaved']]
    else:
        print('Moral principle not recognized or not coded yet - returning ''by_default''')
    #set default index
    result = result.reset_index(drop=True)
    return result
