import numpy as np
import pandas as pd
import math
from .classifier import *
from tqdm import tqdm, tqdm_notebook
from importlib import reload


#codified moral principles applied to the moral machine dataset
#each principle function takes the modified dataframe (by classifer functions) as input and outputs a new dataframe with a new column which contains a moral utility
#the moral utility column contains 1 if the moral principle's criterias are respected, 0 if they aren't and -1 if we can't tell
#there are 3 types of function in this file:
##the "..._utility(x)" function, which output utilites
##the moral principle functions, which output a Dataframe with users and their score to a specific moral principle
##the moral_profile(x) function, which outputs the complete profile for each user according to all moral principles

def SentientUtilitarianism_utility(x):
    #ouputs utility for Utilitarian principle 1 - saves the most lives, whether humans or pets
    y = x
    if y.Lives_saved > y.Lives_notsaved:
        return 1
    if y.Lives_saved == y.Lives_notsaved:
        return -1
    if y.Lives_saved < y.Lives_notsaved:
        return 0
    return 'Error'

def SentientUtilitarianism(x, converted=False, covered_answers=False):
    #Utilitarian principle 1 - saves the most lives, whether humans or pets
    #
    ##the 'converted' argument indicates if the conversion function had previously been applied
    ##this is useful when I want to deal with one specific moral principle without having to deal with useless column
    ##that means that by default, it converts the dataframe and outputs columns that are relevant for this specific moral principle only
    ##the 'converted' argument is only meant to be true when used in the moral_profile function
    #
    if converted:
        y = x
    else:
        y = conversion(x, 'SentientUtilitarianism')
    #this just enables a progress bar to appear while processing, not necessary but useful
    tqdm_notebook().pandas()
    #outptut new columns:
    ##'HowMany_SentientUtilitarianism', which contains the decision he clearly took -- =1 if he saved the most lives, =0 if not
    y['HowMany_SentientUtilitarianism'] = y.progress_apply(SentientUtilitarianism_utility, axis=1)
    ##'Ambiguity_SentientUtilitarianism', which differentiate the situation with ties -- =0 if tie, else =1
    y['Ambiguity_SentientUtilitarianism'] = y['HowMany_SentientUtilitarianism'].progress_apply(lambda x: 0 if x == -1 else 1)
    #transform all '-1' by '0' so that we can easily sum
    y['HowMany_SentientUtilitarianism'] = y['HowMany_SentientUtilitarianism'].progress_apply(lambda x: 0 if x == -1 else x)
    #group by user to calculate average Ambiguity
    Ambiguity = y.groupby('UserID')['Ambiguity_SentientUtilitarianism'].mean().to_frame()
    #another group by user to calculate sums
    howmuch = y.groupby('UserID').sum()
    #among the answers without Ambiguity, how much are following the moral principle
    howmuch['HowMuch_SentientUtilitarianism'] = howmuch.HowMany_SentientUtilitarianism / howmuch.Ambiguity_SentientUtilitarianism
    #drop doulbons before joining the two dataframes
    howmuch = howmuch.drop(columns=['HowMany_SentientUtilitarianism','Ambiguity_SentientUtilitarianism'])
    #join the two dataframes
    result = howmuch.join(Ambiguity)
    #add country information (some of the users put several, we are here taking the first answer)
    country = y[['UserID','UserCountry']]
    country = country.groupby('UserID').first()
    result = result.join(country)
    #invert the Ambiguity for better understanding: Ambiguity contains the percentage of "can't tell" answers
    result['Ambiguity_SentientUtilitarianism'] = result['Ambiguity_SentientUtilitarianism'].progress_apply(lambda x: 1-x)
    #finally, we check the 'converted' argument one last time
    #if we specifically ask this moral principle, it is okay to see the different columns
    #if not, that means we want a full moral profile, and it is better to only see the moral scores
    if converted:
        result = result[['HowMuch_SentientUtilitarianism','Ambiguity_SentientUtilitarianism']]
    if covered_answers:
        #also, we would in that case need to know which answers are not covered by the moral principle
        y = y.set_index('UserID')
        y = y['Ambiguity_SentientUtilitarianism']
        return result, y
    else:
        return result

def ClassicalUtilitarianism_utility(x):
    #ouputs utility for Utilitarian principle 2 - saves the most human lives
    y = x
    if y.Human_saved > y.Human_notsaved:
        return 1
    if y.Human_saved == y.Human_notsaved:
        return -1
    if y.Human_saved < y.Human_notsaved:
        return 0
    return 'Error'

def ClassicalUtilitarianism(x, converted=False, covered_answers=False):
    #Utilitarian principle 2 - saves the most human lives
    #
    ##the 'converted' argument indicates if the conversion function had previously been applied
    ##this is useful when I want to deal with one specific moral principle without having to deal with useless column
    ##that means that by default, it converts the dataframe and outputs columns that are relevant for this specific moral principle only
    ##the 'converted' argument is only meant to be true when used in the moral_profile function
    #
    if converted:
        y = x
    else:
        y = conversion(x, 'ClassicalUtilitarianism')
    #this just enables a progress bar to appear while processing, not necessary but useful
    tqdm_notebook().pandas()
    #outptut new columns:
    ##'HowMany_ClassicalUtilitarianism', which contains the decision he clearly took -- =1 if he saved the most lives, =0 if not
    y['HowMany_ClassicalUtilitarianism'] = y.progress_apply(ClassicalUtilitarianism_utility, axis=1)
    ##'Ambiguity_ClassicalUtilitarianism', which differentiate the situation with ties -- =0 if tie, else =1
    y['Ambiguity_ClassicalUtilitarianism'] = y['HowMany_ClassicalUtilitarianism'].progress_apply(lambda x: 0 if x == -1 else 1)
    #transform all '-1' by '0' so that we can easily sum
    y['HowMany_ClassicalUtilitarianism'] = y['HowMany_ClassicalUtilitarianism'].progress_apply(lambda x: 0 if x == -1 else x)
    #group by user to calculate average Ambiguity
    Ambiguity = y.groupby('UserID')['Ambiguity_ClassicalUtilitarianism'].mean().to_frame()
    #another group by user to calculate sums
    howmuch = y.groupby('UserID').sum()
    #among the answers without Ambiguity, how much are following the moral principle
    howmuch['HowMuch_ClassicalUtilitarianism'] = howmuch.HowMany_ClassicalUtilitarianism / howmuch.Ambiguity_ClassicalUtilitarianism
    #drop doulbons before joining the two dataframes
    howmuch = howmuch.drop(columns=['HowMany_ClassicalUtilitarianism','Ambiguity_ClassicalUtilitarianism'])
    #join the two dataframes
    result = howmuch.join(Ambiguity)
    #add country information (some of the users put several, we are here taking the first answer)
    country = y[['UserID','UserCountry']]
    country = country.groupby('UserID').first()
    result = result.join(country)
    #invert the Ambiguity for better understanding: Ambiguity contains the percentage of "can't tell" answers
    result['Ambiguity_ClassicalUtilitarianism'] = result['Ambiguity_ClassicalUtilitarianism'].progress_apply(lambda x: 1-x)
    #finally, we check the 'converted' argument one last time
    #if we specifically ask this moral principle, it is okay to see the different columns
    #if not, that means we want a full moral profile, and it is better to only see the moral scores
    if converted:
        result = result[['HowMuch_ClassicalUtilitarianism','Ambiguity_ClassicalUtilitarianism']]
    if covered_answers:
        #also, we would in that case need to know which answers are not covered by the moral principle
        y = y.set_index('UserID')
        y = y['Ambiguity_ClassicalUtilitarianism']
        return result, y
    else:
        return result

def HedonisticUtilitarianism_utility(x):
    #ouputs utility for Utilitarian principle 3 - saves the most lives in terms of life expectancy
    ##here we made some assumptions in order to simplify the probelm
    ##assumption 1 : life expectancy is 85 years for all indiviuals
    ##assumption 2 : there are 3 types of people - kids, adults and seniors
    ##assumption 3 : kids are 10, adults are 35 and seniors are 60
    y = x
    if y.Years_saved > y.Years_notsaved:
        return 1
    if y.Years_saved == y.Years_notsaved:
        return -1
    if y.Years_saved < y.Years_notsaved:
        return 0
    return 'Error'

def HedonisticUtilitarianism(x, converted=False, covered_answers=False):
    #Utilitarian principle 3 - saves the most lives in terms of life expectancy
    #
    ##the 'converted' argument indicates if the conversion function had previously been applied
    ##this is useful when I want to deal with one specific moral principle without having to deal with useless column
    ##that means that by default, it converts the dataframe and outputs columns that are relevant for this specific moral principle only
    ##the 'converted' argument is only meant to be true when used in the moral_profile function
    #
    if converted:
        y = x
    else:
        y = conversion(x, 'HedonisticUtilitarianism')
    #this just enables a progress bar to appear while processing, not necessary but useful
    tqdm_notebook().pandas()
    #outptut new columns:
    ##'HowMany_HedonisticUtilitarianism', which contains the decision he clearly took -- =1 if he saved the most lives, =0 if not
    y['HowMany_HedonisticUtilitarianism'] = y.progress_apply(HedonisticUtilitarianism_utility, axis=1)
    ##'Ambiguity_HedonisticUtilitarianism', which differentiate the situation with ties -- =0 if tie, else =1
    y['Ambiguity_HedonisticUtilitarianism'] = y['HowMany_HedonisticUtilitarianism'].progress_apply(lambda x: 0 if x == -1 else 1)
    #transform all '-1' by '0' so that we can easily sum
    y['HowMany_HedonisticUtilitarianism'] = y['HowMany_HedonisticUtilitarianism'].progress_apply(lambda x: 0 if x == -1 else x)
    #group by user to calculate average Ambiguity
    Ambiguity = y.groupby('UserID')['Ambiguity_HedonisticUtilitarianism'].mean().to_frame()
    #another group by user to calculate sums
    howmuch = y.groupby('UserID').sum()
    #among the answers without Ambiguity, how much are following the moral principle
    howmuch['HowMuch_HedonisticUtilitarianism'] = howmuch.HowMany_HedonisticUtilitarianism / howmuch.Ambiguity_HedonisticUtilitarianism
    #drop doulbons before joining the two dataframes
    howmuch = howmuch.drop(columns=['HowMany_HedonisticUtilitarianism','Ambiguity_HedonisticUtilitarianism'])
    #join the two dataframes
    result = howmuch.join(Ambiguity)
    #add country information (some of the users put several, we are here taking the first answer)
    country = y[['UserID','UserCountry']]
    country = country.groupby('UserID').first()
    result = result.join(country)
    #invert the Ambiguity for better understanding: Ambiguity contains the percentage of "can't tell" answers
    result['Ambiguity_HedonisticUtilitarianism'] = result['Ambiguity_HedonisticUtilitarianism'].progress_apply(lambda x: 1-x)
    #finally, we check the 'converted' argument one last time
    #if we specifically ask this moral principle, it is okay to see the different columns
    #if not, that means we want a full moral profile, and it is better to only see the moral scores
    if converted:
        result = result[['HowMuch_HedonisticUtilitarianism','Ambiguity_HedonisticUtilitarianism']]
        #also, we would in that case need to know which answers are not covered by the moral principle
    if covered_answers:
        y = y.set_index('UserID')
        y = y['Ambiguity_HedonisticUtilitarianism']
        return result, y
    else:
        return result

def AgentCenteredDeontology_utility(x):
    #ouputs utility for AgentCenteredDeontology principle - duty ethics, chooses self-sacrifice over involving pedestrians
    y = x
    if y.Barrier_saved == '1':
        return 1
    if y.Barrier_notsaved == '1':
        return 0
    if y.Barrier_saved == '0' and y.Barrier_notsaved == '0':
        return -1
    return 'Error'

def AgentCenteredDeontology(x, converted=False, covered_answers=False):
    #AgentCenteredDeontology principle - duty ethics, chooses self-sacrifice over involving pedestrians
    #
    ##the 'converted' argument indicates if the conversion function had previously been applied
    ##this is useful when I want to deal with one specific moral principle without having to deal with useless column
    ##that means that by default, it converts the dataframe and outputs columns that are relevant for this specific moral principle only
    ##the 'converted' argument is only meant to be true when used in the moral_profile function
    #
    if converted:
        y = x
    else:
        y = conversion(x, 'AgentCenteredDeontology')
    #this just enables a progress bar to appear while processing, not necessary but useful
    tqdm_notebook().pandas()
    #outptut new columns:
    ##'HowMany_AgentCenteredDeontology', which contains the decision he clearly took -- =1 if he saved the most lives, =0 if not
    y['HowMany_AgentCenteredDeontology'] = y.progress_apply(AgentCenteredDeontology_utility, axis=1)
    ##'Ambiguity_AgentCenteredDeontology', which differentiate the situation with ties -- =0 if tie, else =1
    y['Ambiguity_AgentCenteredDeontology'] = y['HowMany_AgentCenteredDeontology'].progress_apply(lambda x: 0 if x == -1 else 1)
    #transform all '-1' by '0' so that we can easily sum
    y['HowMany_AgentCenteredDeontology'] = y['HowMany_AgentCenteredDeontology'].progress_apply(lambda x: 0 if x == -1 else x)
    #group by user to calculate average Ambiguity
    Ambiguity = y.groupby('UserID')['Ambiguity_AgentCenteredDeontology'].mean().to_frame()
    #another group by user to calculate sums
    howmuch = y.groupby('UserID').sum()
    #among the answers without Ambiguity, how much are following the moral principle
    howmuch['HowMuch_AgentCenteredDeontology'] = howmuch.HowMany_AgentCenteredDeontology / howmuch.Ambiguity_AgentCenteredDeontology
    #drop doulbons before joining the two dataframes
    howmuch = howmuch.drop(columns=['HowMany_AgentCenteredDeontology','Ambiguity_AgentCenteredDeontology'])
    #join the two dataframes
    result = howmuch.join(Ambiguity)
    #add country information (some of the users put several, we are here taking the first answer)
    country = y[['UserID','UserCountry']]
    country = country.groupby('UserID').first()
    result = result.join(country)
    #invert the Ambiguity for better understanding: Ambiguity contains the percentage of "can't tell" answers
    result['Ambiguity_AgentCenteredDeontology'] = result['Ambiguity_AgentCenteredDeontology'].progress_apply(lambda x: 1-x)
    #finally, we check the 'converted' argument one last time
    #if we specifically ask this moral principle, it is okay to see the different columns
    #if not, that means we want a full moral profile, and it is better to only see the moral scores
    if converted:
        result = result[['HowMuch_AgentCenteredDeontology','Ambiguity_AgentCenteredDeontology']]
        #also, we would in that case need to know which answers are not covered by the moral principle
    if covered_answers:
        y = y.set_index('UserID')
        y = y['Ambiguity_AgentCenteredDeontology']
        return result, y
    else:
        return result


def PatientCenteredDeontology_utility(x):
    #ouputs utility for PatientCenteredDeontology principle - rights-based ethics, chooses self-sacrifice over involving pedestrians, when not possible avoid swerving the car
    y = x
    if y.Intervention_saved == '1':
        return 0
    if y.Intervention_saved == '0':
        return 1

def PatientCenteredDeontology(x, converted=False, covered_answers=False):
    #PatientCenteredDeontology principle - rights-based ethics, chooses self-sacrifice over involving pedestrians, when not possible avoid swerving the car
    #
    ##the 'converted' argument indicates if the conversion function had previously been applied
    ##this is useful when I want to deal with one specific moral principle without having to deal with useless column
    ##that means that by default, it converts the dataframe and outputs columns that are relevant for this specific moral principle only
    ##the 'converted' argument is only meant to be true when used in the moral_profile function
    #
    if converted:
        y = x
    else:
        y = conversion(x, 'PatientCenteredDeontology')
    #this just enables a progress bar to appear while processing, not necessary but useful
    tqdm_notebook().pandas()
    #outptut new columns:
    ##'HowMany_PatientCenteredDeontology', which contains the decision he clearly took -- =1 if he saved the most lives, =0 if not
    y['HowMany_PatientCenteredDeontology'] = y.progress_apply(PatientCenteredDeontology_utility, axis=1)
    ##'Ambiguity_PatientCenteredDeontology', which differentiate the situation with ties -- =0 if tie, else =1
    y['Ambiguity_PatientCenteredDeontology'] = y['HowMany_PatientCenteredDeontology'].progress_apply(lambda x: 0 if x == -1 else 1)
    #transform all '-1' by '0' so that we can easily sum
    y['HowMany_PatientCenteredDeontology'] = y['HowMany_PatientCenteredDeontology'].progress_apply(lambda x: 0 if x == -1 else x)
    #group by user to calculate average Ambiguity
    Ambiguity = y.groupby('UserID')['Ambiguity_PatientCenteredDeontology'].mean().to_frame()
    #another group by user to calculate sums
    howmuch = y.groupby('UserID').sum()
    #among the answers without Ambiguity, how much are following the moral principle
    howmuch['HowMuch_PatientCenteredDeontology'] = howmuch.HowMany_PatientCenteredDeontology / howmuch.Ambiguity_PatientCenteredDeontology
    #drop doulbons before joining the two dataframes
    howmuch = howmuch.drop(columns=['HowMany_PatientCenteredDeontology','Ambiguity_PatientCenteredDeontology'])
    #join the two dataframes
    result = howmuch.join(Ambiguity)
    #add country information (some of the users put several, we are here taking the first answer)
    country = y[['UserID','UserCountry']]
    country = country.groupby('UserID').first()
    result = result.join(country)
    #invert the Ambiguity for better understanding: Ambiguity contains the percentage of "can't tell" answers
    result['Ambiguity_PatientCenteredDeontology'] = result['Ambiguity_PatientCenteredDeontology'].progress_apply(lambda x: 1-x)
    #finally, we check the 'converted' argument one last time
    #if we specifically ask this moral principle, it is okay to see the different columns
    #if not, that means we want a full moral profile, and it is better to only see the moral scores
    if converted:
        result = result[['HowMuch_PatientCenteredDeontology','Ambiguity_PatientCenteredDeontology']]
        #also, we would in that case need to know which answers are not covered by the moral principle
    if covered_answers:
        y = y.set_index('UserID')
        y = y['Ambiguity_PatientCenteredDeontology']
        return result, y
    else:
        return result

def ContractarianDeontology_utility(x):
    #ouputs utility for ContractarianDeontology principle - duty ethics, chooses to save pedestrians legally crossing the street whenever possible
    y = x
    if y.CrossingSignal_saved == '0':
        if y.CrossingSignal_notsaved == '0':
            return -1
        if y.CrossingSignal_notsaved == '1':
            return -1
        if y.CrossingSignal_notsaved == '2':
            return 1
    if y.CrossingSignal_saved == '1':
        if y.CrossingSignal_notsaved == '0':
            return -1
        if y.CrossingSignal_notsaved == '1':
            return -1
        if y.CrossingSignal_notsaved == '2':
            return 1
    if y.CrossingSignal_saved == '2':
        if y.CrossingSignal_notsaved == '0':
            return 0
        if y.CrossingSignal_notsaved == '1':
            return 0
        if y.CrossingSignal_notsaved == '2':
            return -1

def ContractarianDeontology(x, converted=False, covered_answers=False):
    #ContractarianDeontology principle - duty ethics, chooses to save pedestrians legally crossing the street whenever possible
    #
    ##the 'converted' argument indicates if the conversion function had previously been applied
    ##this is useful when I want to deal with one specific moral principle without having to deal with useless column
    ##that means that by default, it converts the dataframe and outputs columns that are relevant for this specific moral principle only
    ##the 'converted' argument is only meant to be true when used in the moral_profile function
    #
    if converted:
        y = x
    else:
        y = conversion(x, 'ContractarianDeontology')
    #this just enables a progress bar to appear while processing, not necessary but useful
    tqdm_notebook().pandas()
    #outptut new columns:
    ##'HowMany_ContractarianDeontology', which contains the decision he clearly took -- =1 if he saved the most lives, =0 if not
    y['HowMany_ContractarianDeontology'] = y.progress_apply(ContractarianDeontology_utility, axis=1)
    ##'Ambiguity_ContractarianDeontology', which differentiate the situation with ties -- =0 if tie, else =1
    y['Ambiguity_ContractarianDeontology'] = y['HowMany_ContractarianDeontology'].progress_apply(lambda x: 0 if x == -1 else 1)
    #transform all '-1' by '0' so that we can easily sum
    y['HowMany_ContractarianDeontology'] = y['HowMany_ContractarianDeontology'].progress_apply(lambda x: 0 if x == -1 else x)
    #group by user to calculate average Ambiguity
    Ambiguity = y.groupby('UserID')['Ambiguity_ContractarianDeontology'].mean().to_frame()
    #another group by user to calculate sums
    howmuch = y.groupby('UserID').sum()
    #among the answers without Ambiguity, how much are following the moral principle
    howmuch['HowMuch_ContractarianDeontology'] = howmuch.HowMany_ContractarianDeontology / howmuch.Ambiguity_ContractarianDeontology
    #drop doulbons before joining the two dataframes
    howmuch = howmuch.drop(columns=['HowMany_ContractarianDeontology','Ambiguity_ContractarianDeontology'])
    #join the two dataframes
    result = howmuch.join(Ambiguity)
    #add country information (some of the users put several, we are here taking the first answer)
    country = y[['UserID','UserCountry']]
    country = country.groupby('UserID').first()
    result = result.join(country)
    #invert the Ambiguity for better understanding: Ambiguity contains the percentage of "can't tell" answers
    result['Ambiguity_ContractarianDeontology'] = result['Ambiguity_ContractarianDeontology'].progress_apply(lambda x: 1-x)
    #finally, we check the 'converted' argument one last time
    #if we specifically ask this moral principle, it is okay to see the different columns
    #if not, that means we want a full moral profile, and it is better to only see the moral scores
    if converted:
        result = result[['HowMuch_ContractarianDeontology','Ambiguity_ContractarianDeontology']]
    if covered_answers:
        #also, we would in that case need to know which answers are not covered by the moral principle
        y = y.set_index('UserID')
        y = y['Ambiguity_ContractarianDeontology']
        return result, y
    else:
        return result


def uncovered_answers(x):
    ##outputs a new column with answers that aren't explained with any of our principles (neither pro or against)
    y = x
    if y.Ambiguity_HedonisticUtilitarianism == 1 and y.Ambiguity_ClassicalUtilitarianism == 1 and y.Ambiguity_AgentCenteredDeontology == 1 and y.Ambiguity_SentientUtilitarianism == 1 and y.Ambiguity_PatientCenteredDeontology == 1 and y.Ambiguity_ContractarianDeontology == 1:
        return 1
    return 0

def moral_profile(x, converted=False):
    #outputs a dataframe with a column for each moral principle
    if converted:
        y = x
        ##then applies the moral principles
        [u_SentientUtilitarianism,uncertain_SentientUtilitarianism] = SentientUtilitarianism(y, converted = True, covered_answers=True)
        [u_ClassicalUtilitarianism,uncertain_ClassicalUtilitarianism] = ClassicalUtilitarianism(y, converted = True, covered_answers=True)
        [u_HedonisticUtilitarianism,uncertain_HedonisticUtilitarianism] = HedonisticUtilitarianism(y, converted = True, covered_answers=True)
        [d_AgentCenteredDeontology,uncertain_AgentCenteredDeontology] = AgentCenteredDeontology(y, converted = True, covered_answers=True)
        [d_PatientCenteredDeontology,uncertain_PatientCenteredDeontology] = PatientCenteredDeontology(y, converted = True, covered_answers=True)
        [d_ContractarianDeontology,uncertain_ContractarianDeontology] = ContractarianDeontology(y, converted = True, covered_answers=True)
    else:
        ##first converts the dataframe to have the maximum info possible
        y = conversion(x)
        ##then applies the moral principles
        [u_SentientUtilitarianism,uncertain_SentientUtilitarianism] = SentientUtilitarianism(y, converted = True, covered_answers=True)
        [u_ClassicalUtilitarianism,uncertain_ClassicalUtilitarianism] = ClassicalUtilitarianism(y, converted = True, covered_answers=True)
        [u_HedonisticUtilitarianism,uncertain_HedonisticUtilitarianism] = HedonisticUtilitarianism(y, converted = True, covered_answers=True)
        [d_AgentCenteredDeontology,uncertain_AgentCenteredDeontology] = AgentCenteredDeontology(y, converted = True, covered_answers=True)
        [d_PatientCenteredDeontology,uncertain_PatientCenteredDeontology] = PatientCenteredDeontology(y, converted = True, covered_answers=True)
        [d_ContractarianDeontology,uncertain_ContractarianDeontology] = ContractarianDeontology(y, converted = True, covered_answers=True)
    #do not forget to add 'UserCountry' at the end
    country = y[['UserID','UserCountry']]
    country = country.groupby('UserID').first()
    #join all of them into one magical dataframe
    globalPrinciples = [u_SentientUtilitarianism, u_ClassicalUtilitarianism, u_HedonisticUtilitarianism, d_AgentCenteredDeontology, d_PatientCenteredDeontology, d_ContractarianDeontology, country]
    globalAmbiguity = [uncertain_SentientUtilitarianism, uncertain_ClassicalUtilitarianism, uncertain_HedonisticUtilitarianism, uncertain_AgentCenteredDeontology, uncertain_PatientCenteredDeontology, uncertain_ContractarianDeontology]
    return globalPrinciples, globalAmbiguity



def fix_this(gA):
    r_A1 = gA[0].add(gA[1])
    r_A2 = r_A1.add(gA[2])
    r_A3 = r_A2.add(gA[3])
    r_A4 = r_A3.add(gA[4])
    r_A5 = r_A4.add(gA[5])
    r_A6 = r_A5.divide(6)
    r_A7 = r_A6.floordiv(1)
    r_A8 = r_A7.to_frame(name="Uncovered_answers")
    r_A9 = r_A8.groupby(level="UserID").mean()
    return r_A9




def moral_profile2(globalPrinciples, globalAmbiguity):
    result = pd.concat(globalPrinciples, axis=1, join='inner')

    #globalAmbiguity2 = [df[~df.index.duplicated()] for df in globalAmbiguity]
    #result_Ambiguity = pd.concat(globalAmbiguity2, axis=1, join='inner')

    r_A = fix_this(globalAmbiguity)

    # b = [y.groupby('UserID') for y in globalAmbiguity]
    # df_ua = pd.DataFrame(b, columns=["Uncovered_answers"])
    # print(df_ua)
    #result_Ambiguity = result_Ambiguity.join(df_ua)




    ##outputs a new column with answers that don't fit with any principle
    #result_Ambiguity['Uncovered_answers'] = result_Ambiguity.progress_apply(uncovered_answers, axis=1)
    #result_Ambiguity = pd.concat([result_Ambiguity, r_A], axis=1, join="inner")
    #saves a new dataframe with uncovered answers by users
    #result_Ambiguity = result_Ambiguity.groupby('UserID')['Uncovered_answers'].mean().to_frame()
    final_joint = pd.concat([result, r_A], axis=1)
    #keeping the scores only
    moral_scores = ['UserCountry','Uncovered_answers','HowMuch_SentientUtilitarianism','Ambiguity_SentientUtilitarianism','HowMuch_ClassicalUtilitarianism','Ambiguity_ClassicalUtilitarianism','HowMuch_HedonisticUtilitarianism','Ambiguity_HedonisticUtilitarianism','HowMuch_AgentCenteredDeontology','Ambiguity_AgentCenteredDeontology','HowMuch_PatientCenteredDeontology','Ambiguity_PatientCenteredDeontology','HowMuch_ContractarianDeontology','Ambiguity_ContractarianDeontology']
    final_profile = final_joint[moral_scores]
    return final_profile
